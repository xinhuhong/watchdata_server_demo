import uuid
import json
from flask import Flask, request, jsonify

app = Flask(__name__)
counter = 0

@app.route('/')
def index():
    return 'SyncSign Example Server'

def buildContent():
    """ Create the content to show on the eink screen.
        Reference: https://dev.sync-sign.com/hubsdk/guides/render_layout.html
        Returns:
            A dict(json) contains the render document.
    """
    return {
            "items": [
                {   "type": "TEXT",
                    "data": { "text": "HELLO WORLD #" + str(counter),
                            "font": "YANONE_KAFFEESATZ_44_B",
                            "block": {"x": 24, "y": 30, "w": 496, "h": 60}
                    }
                }
            ]
        }

@app.route('/renders/node/<string:nodeId>', methods=['GET'])
def rendersContent(nodeId):
    """ Route handler of reading render's content.
        Used to download the new content, if any.
        Args:
            nodeId: specify which node is requesting for new content.
        Returns:
            The response with renderId and content to remote peer.
    """
    global counter
    counter += 1
    if counter % 3 == 0:
        res = { "code": 204, "data": [] }  # Mock a situation when there is no new content to refresh
    else:
        renderId = str(uuid.uuid4())  # Generate a unique ID, to identify each content
        print("Generating new content for node:", nodeId, "using render ID:", renderId)
        res = { "code": 200,
                "data": [
                    {   "renderId": renderId,      # Must unique for each content
                        "nodeId": nodeId,          # Must include for remote peer
                        "isRendered": False,       # Set to False, means this is a new content
                        "content": buildContent()  # The layout document to show on the screen
                    }
                ]
            }
    return jsonify(res)

@app.route('/renders/id/<string:renderId>', methods=['PUT'])
def rendersReport(renderId):
    """ Route handler of receiving render's report.
        Used to report the rendering result.
        Args:
            renderId: report result for a previous unique ID.
        Returns:
            The empty response to remote peer.
    """
    # Receving render result report (renderId is the same as previous GET request)
    print('Render ID:', renderId, 'reported', "success" if request.get_json()["data"]["isRendered"] else "failure")
    res = { "code": 204 }
    return jsonify(res)

@app.route('/nodeInfo', methods=['PUT'])
def nodeInfoReport():
    """ Route handler of receiving nodeInfo report.
    """
    # Receving render result report (renderId is the same as previous GET request)
    nodeInfo = request.get_json()
    # EXAMPLE: nodeInfo = {'nodes': [{'nodeId': '004C11AEBC36B400', 'batteryLevel': 93}]}
    print('nodeId:', nodeInfo["nodes"][0]["nodeId"], ' batteryLevel:', nodeInfo["nodes"][0]["batteryLevel"])
    res = { "code": 204 }
    return jsonify(res) 

if __name__ == '__main__':
    app.run(host=("0.0.0.0"))