from flask import Flask, render_template, request, redirect, url_for,jsonify
import json
app = Flask(__name__)

Post_the_following_format = {
  "Request": "Read",
  "DbName": "Gateway",
  "Condition": {
    "BranchID": "B2103002-01",
    "WatchID": "VW-0123"
  }
}
request_Post_the_following_format1 = {
  "DbName": "Gateway",
  "Condition": {
    "BranchID": "B2103002-01",
    "WatchID": "VW-0123"
  },
  "DbStruct": [
    { "BranchID": "B2103002-01",
      "WatchID": "VW-0123",
      "PersonalID": "P2103001",
      "Function": 1,
      "Version": "1.00",
      "Date": "2021-12-15T12:03:24.0052653+10:00"
    }
  ]
}

request_Post_the_following_format2 = {
  "DbName": "RecPR",
  "Condition": {
    "BranchID": "B2103002-01",
    "PersonalID": "P2103001"
  },
  "DbStruct": "OK"
}

request_Post_the_following_format3 = {
  "DbName": "RecPR",
  "Condition": {
    "BranchID": "B2103002-01",
    "PersonalID": "P2103001",
    "DateTime": "LAST"
  },
  "DbStruct": [
    {
      "BranchID": "B2103002-01",
      "PersonalID": "P2103001",
      "DateTime": "2021-12-15T12:44:54.5010146+09:00",
      "Battery": 99.92,
      "OnBody": 'true',
      "PR": 145.75,
      "BT": 37.32,
      "O2": 99.1,
      "BPH": 139.54,
      "BPL": 89.23,
      "FS": 10,
      "SLP": 0
    }
  ]
}


@app.route('/api1', methods=['POST', 'GET'])
def report_gateway_settings():
    if request.method == "POST":
        Request = request.form['Request']
        DbName = request.form['DbName']
        Condition = request.form['Condition']
        return redirect(url_for('/showbio',
                                Request=Request,
                                DbName=DbName,
                                Condition=Condition,
                                ))
    return render_template("bio_form1.html")

@app.route('/showbio', methods=['GET'])
def showbio():
    Request = request.args.get('Request')
    DbName = request.args.get('DbName')
    Condition = request.args.get('Condition')
    try:
        Server_request_1 = request_Post_the_following_format1
        print('服务器接口1已接收客户端数据:\n'
              '"Request":{}\n'
              '"DbName": {}\n'
              '"Condition":{}'.format(Request,DbName,Condition))
        return render_template("show_bio1.html",
                               #Request=Request1,
                               #DbName=Condition1,
                               Condition=Server_request_1,)
    except:
        print('error:not request_Post_the_following_format', 500)


@app.route('/api2', methods=['POST', 'GET'])
def report_gateway_settings2():
    if request.method == "POST":
        Request = request.form['Request']
        DbName = request.form['DbName']
        Condition = request.form['Condition']
        return redirect(url_for('/showbio2',
                                Request=Request,
                                DbName=DbName,
                                Condition2=Condition,
                                ))
    return render_template("bio_from2.html")


@app.route('/showbio2', methods=['GET'])
def showbio3():
    Request = request.args.get('Request')
    DbName = request.args.get('DbName')
    Condition = request.args.get('Condition')
    try:
        Server_request_2 = request_Post_the_following_format2
        print('服务器接口2已接收客户端数据:\n'
              '"Request":{}\n'
              '"DbName": {}\n'
              '"Condition":{}'.format(Request, DbName, Condition))
        return render_template("show_bio2.html",
                               # Request=Request1,
                               # DbName=Condition1,
                               Condition2=Server_request_2, )
    except:
        print('error:not request_Post_the_following_format', 500)


@app.route('/api3', methods=['POST', 'GET'])
def report_gateway_settings3():
    if request.method == "POST":
        Request = request.form['Request']
        DbName = request.form['DbName']
        Condition = request.form['Condition']
        return redirect(url_for('/showbio3',
                                Request=Request,
                                DbName=DbName,
                                Condition2=Condition,
                                ))
    return render_template("bio_from3.html")


@app.route('/showbio3', methods=['GET'])
def showbio2():
    Request = request.args.get('Request')
    DbName = request.args.get('DbName')
    Condition = request.args.get('Condition')
    try:
        Server_request_3 = request_Post_the_following_format3
        print('服务器接口3已接收客户端数据:\n'
              '"Request":{}\n'
              '"DbName": {}\n'
              '"Condition":{}'.format(Request, DbName, Condition))
        return render_template("show_bio3.html",
                               # Request=Request1,
                               # DbName=Condition1,
                               Condition3=Server_request_3, )
    except:
        print('error:not request_Post_the_following_format', 500)










if __name__ =="__main__":
    app.run(debug=True,port=8080)
