from flask import Flask, render_template, request, redirect, url_for,jsonify
import json
app = Flask(__name__)

@app.route('/hellow')
def hello_world():

    return render_template('index.html', name="Stupid")



@app.route('/form', methods=['POST', 'GET'])
def bio_data_form():
    if request.method == "POST":
        username = request.form['username']
        age = request.form['age']
        email = request.form['email']
        hobbies = request.form['hobbies']
        return redirect(url_for('/showbio',
                                username=username,
                                age=age,
                                email=email,
                                hobbies=hobbies))

    return render_template("bio_form.html")




@app.route('/showbio', methods=['GET'])
def showbio():
    username = request.args.get('username')
    age = request.args.get('age')
    email = request.args.get('email')
    hobbies = request.args.get('hobbies')
    return render_template("show_bio.html",
                           username=username,
                           age=age,
                           email=email,
                           hobbies=hobbies)



@app.route("/login", methods=['POST'])
def login():
 data = request.get_data()
 data = json.loads(data)
 #username = data['username']
 #password = data['password']

 return jsonify({
  "Request": "Read",
  "DbName": "Gateway",
  "Condition": {
    "BranchID": "B2103002-01",
    "WatchID": "VW-0123"
  }
}
) # 返回布尔值














if __name__ =="__main__":
    app.run(debug=True,port=8080)
