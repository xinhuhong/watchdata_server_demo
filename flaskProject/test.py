
import re, json, time

pattern = ('www')
string = {
  'Request': 'Read',
  'DbName': 'Gateway',
  'Condition': {
    'BranchID': 'B2103002-01',
    'WatchID': 'VW-0123'
  }}


import time
def time_click(func):
    def inner():
        start = time.time()
        func()
        end = time.time()
        print(end-start)
    return inner


@time_click
def jjson():
    print(string)
    a = json.dumps(string)
    print(a)
    time.sleep(2)
    b = json.loads(a)
    print(b)

jjson()
---------------------------



